<?php
namespace Home\Controller;
use Think\Controller;
class CommonController extends Controller
{
    //判断两课程上课时间是否冲突
    static function equal($a,$b)
    {
        if($a==$b)
            return 1;
        else if($a=='周一第11.12节'&&$b=='周一第11.12.13节')
            return 1;
        else if($a=='周二第11.12节'&&$b=='周二第11.12.13节')
            return 1;
        else if($a=='周三第11.12节'&&$b=='周三第11.12.13节')
            return 1;
        else if($a=='周四第11.12节'&&$b=='周四第11.12.13节')
            return 1;
        else if($a=='周五第11.12节'&&$b=='周五第11.12.13节')
            return 1;
        else
            return 0;
    }
    //加密函数
    static function lock_url($txt,$key='your love is my love'){
        $chars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-=+";
        $nh=rand(0,64);
        $ch=$chars[$nh];
        $mdkey=md5($key.$ch);
        $mdkey=substr($mdkey,$nh%8, $nh%8+7);
        $txt=base64_encode($txt);
        $tmp= '';
        $i=0;
        $j=0;
        $k=0;
        for($i=0;$i<strlen($txt);$i++) {
            $k=$k==strlen($mdkey)?0:$k;
            $j=($nh+strpos($chars,$txt[$i])+ord($mdkey[$k++]))%64;
            $tmp.=$chars[$j];
        }
        return urlencode($ch.$tmp);
    }
    //解密函数
    static function unlock_url($txt,$key='your love is my love'){
        $txt=urldecode($txt);
        $chars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-=+";
        $ch=$txt[0];
        $nh=strpos($chars,$ch);
        $mdkey=md5($key.$ch);
        $mdkey=substr($mdkey,$nh%8,$nh%8+7);
        $txt=substr($txt,1);
        $tmp='';
        $i=0;$j=0;$k=0;
        for($i=0;$i<strlen($txt);$i++) {
            $k=$k==strlen($mdkey)?0:$k;
            $j=strpos($chars,$txt[$i])-$nh-ord($mdkey[$k++]);
            while($j<0) $j+=64;
            $tmp.=$chars[$j];
        }
        return base64_decode($tmp);
    }
}