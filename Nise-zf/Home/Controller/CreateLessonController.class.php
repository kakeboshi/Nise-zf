<?php
namespace Home\Controller;
use Think\Controller;
class CreateLessonController extends Controller
{
    public function index()
    {
        $this->display('create');
    }
    public function create()
    {
        if(session('teacher'))//判断教师是否登录
        {
            $teacher=session('teacher');//获取教师信息
            if($_POST['lessonname']&&$_POST['lessontime']&&$_POST['lessonroom']&&$_POST['totalvolume'])//判断填写课程信息的完整性
            {
                $lesson['lessonName']=$_POST['lessonname'];
                $lesson['lessonTime']=$_POST['lessontime'];
                $lesson['lessonRoom']=$_POST['lessonroom'];
                $lesson['totalVolume']=$_POST['totalvolume'];
                $lesson['leftVolume']=$_POST['totalvolume'];
                $lesson['teacherName']=$_SESSION['teacher']['teachername'];
                $lesson['teacherId']=$_SESSION['teacher']['teacherid'];//建立课程信息
                $lessons=M('totallesson')->where('teacherId=%d',$teacher['teacherid'])->select();//获取教师已创建的所有课程
                $flag=0;
                foreach ($lessons as $time)//判断当前课程和教师已创建课程是否有时间冲突
                {
                    if($lesson['lessonTime']==$time['lessontime'])
                        $flag=1;
                }
                $lessons=M('totallesson')->where('lessonTime=%d',$lesson['lessonTime'])->select();//获取当前的所有课程
                foreach($lessons as $temp)//判断当前课程和所有课程是否存在时间和场所的冲突
                {
                    if(CommonController::equal($lesson['lessonTime'],$temp['lessontime'])&&$lesson['lessonRoom']==$temp['lessonroom'])
                        $flag=1;
                }
                if($flag==0)//将新课程写入数据库
                {
                    M('totallesson')->create($lesson);
                    $tip = M('totallesson')->add($lesson);
                    if ($tip)
                    {
                        $this->success('成功增加一门课程');
                    }
                    else
                    {
                        $this->error('创建课程失败，请稍后再试');
                    }
                }
                else
                {
                    $this->error('你在这个时段有课或该课室在该时段已经被占用，请重新填写课程信息');
                }
            }
            else
            {
                $this->error('请完整填写课程信息');
            }
        }
        else
        {
            $this->error('你还没有登录，请登录','http://localhost/Nise-zf/index.php/Home/TeacherLogin/index.html');
        }
    }
}