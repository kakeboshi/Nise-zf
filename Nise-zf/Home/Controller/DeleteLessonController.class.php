<?php
namespace Home\Controller;
use Think\Controller;
class DeleteLessonController extends Controller
{
    public function delete()
    {
        if(cookie('teacher'))//判断教师是否登陆
        {
            $DeleteId=$_POST['lessonId'];//获取要删除的课程的信息
            $tip1=M('totallesson')->where('lessonId=%d',$DeleteId)->delete();//删除总课程表中的记录
            $tip2=M('chooselesson')->where('lessonId=%d',$DeleteId)->delete();//删除选课表中的记录
            if($tip1&&$tip2)//返回信息
                $this->success('删除课程成功');
            else
                $this->error('删除课程失败');
        }
        else
        {
            $this->error('你还没有登录，请登录','http://localhost/Nise-zf/index.php/Home/TeacherLogin/index.html');
        }
    }
}