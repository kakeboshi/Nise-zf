<?php
namespace Home\Controller;
use Think\Controller;
class DropClassController extends Controller
{
    public function drop()
    {
        if(cookie('student'))//判断学生是否登录
        {
            $dropId=$_POST['classId'];//获取要退选的课程信息
            $student=$_SESSION['student'];//获取学生身份
            M('chooselesson')->where('lessonId=%d&&studentId=%d',$dropId,$student['studentid'])->delete();//删除选课表中的信息
            $class=M('totallesson')->where('lessonId=%d',$dropId)->select();//在总课表中增加该课程的余量
            $update['leftVolume']=$class[0]['leftvolume']+1;
            $tip=M('totallesson')->where('lessonId=%d',$dropId)->save($update);
            if($tip)//打印信息
                $this->success('退课成功');
            else
                $this->error('退课失败，请稍后再试');
        }
        else
        {
            $this->error('你还没有登录，请登录','http://localhost/Nise-zf/index.php/Home/StudentLogin/index.html');
        }
    }
}