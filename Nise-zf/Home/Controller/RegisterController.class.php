<?php
namespace Home\Controller;
use Think\Controller;
class RegisterController extends Controller
{
    public function index()
    {
        $this->display('register');
    }
    public function register()
    {
        //判断注册信息是否完整
        if($_POST['userName']&&$_POST['EmailAddress']&&$_POST['password']&&$_POST['repassword']&&($_POST['password']==$_POST['repassword'])&&$_POST['kind'])
        {
            $user['password']=CommonController::lock_url($_POST['password']);//加密密码
            if($_POST['kind']=='教师')//确定用户身份，并写入对应用户表
            {
                $user['teacherName']=$_POST['userName'];
                $user['EmailAddress']=$_POST['EmailAddress'];
                M('teacher')->create($user);
                $tip=M('teacher')->add($user);
                if($tip)
                    $this->success('注册成功','http://localhost/Nise-zf/index.php');
                else
                    $this->error('注册失败');
            }
            if($_POST['kind']=='学生')//确定用户身份，并写入用户表
            {
                $user['studentName']=$_POST['userName'];
                $user['EmailAddress']=$_POST['EmailAddress'];
                M('student')->create($user);
                $tip=M('student')->add($user);
                if($tip)
                    $this->success('注册成功','http://localhost/Nise-zf/index.php');
                else
                    $this->error('注册失败');
            }
        }
        else
        {
            $this->error('请完整填写资料，或该邮箱已被注册');
        }
    }
}