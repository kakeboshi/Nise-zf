<?php
namespace Home\Controller;
use Think\Controller;
class StudentController extends Controller
{
    public function index()
    {
        $this->display('page');
    }
    public function page()
    {
        if(cookie('student'))
        {
            $Student=$_SESSION['student'];//获取学生信息，并传递给模板
            $this->assign('student',$Student);
            $ChooseLessons=M('chooselesson')->where('studentId=%d',$Student['studentid'])->select();//获取学生选课信息，并传递给模板
            $n=0;
            $Lesson;
            foreach ($ChooseLessons as $temp)
            {
                $Lesson[$n]=M('totallesson')->where('lessonId=%d',$temp['lessonid'])->select();
                $Lesson[$n]=$Lesson[$n][0];
                ++$n;
            }
            $this->assign('lesson',$Lesson);
            $this->display();
        }
        else
        {
            $this->error('你还没有登录，请登录','http://localhost/Nise-zf/index.php/Home/StudentLogin/index.html');
        }
    }
}