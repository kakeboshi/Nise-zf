<?php
namespace Home\Controller;
use Think\Controller;
class StudentLoginController extends Controller
{
    public function index()
    {
        $this->display('login');
    }
    public function login()
    {
        if($_POST['email']&&$_POST['password'])//判断登录信息是否填写完整
        {
            $email=$_POST['email'];
            $password=$_POST['password'];
            $data=M('student')->where('emailaddress=\'%s\'',$email)->select();//从数据库中查询是否有该用户
            $pass=CommonController::unlock_url($data[0]['password']);//解密密码
            if($password==$pass)//判断密码是否正确
            {
                cookie('student',$data[0],time()+3600);
                session('student',$data[0]);
                $this->success("登录成功，欢迎你，".$data[0]['studentname']."同学",'http://localhost/Nise-zf/index.php/Home/Student/page');
            }
            else
            {
                $this->error("输入的账号或密码有误！");
            }
        }
        else
        {
            $this->error("请填写完整的登录信息",'http://localhost/Nise-zf/index.php/Home/StudentLogin/index.html');
        }
    }
}