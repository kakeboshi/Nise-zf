<?php
namespace Home\Controller;
use Think\Controller;
class StudentLogoutController extends Controller
{
    public function logout()
    {
        $value=cookie('student');
        cookie('student',null);//清空cookie
        session('student',null);//清空session
        $this->success("成功退出，请重新登录", U('StudentLogin/index'));
    }
}