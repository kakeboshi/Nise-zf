<?php
namespace Home\Controller;
use Think\Controller;
class TeacherController extends Controller
{
    public function page()
    {
        if(cookie('teacher'))//判断教师是否登录
        {
            $Teacher=$_SESSION['teacher'];//获取教师信息，并传递给模板
            $this->assign('teacher',$Teacher);
            $CreatedLessons=M('totallesson')->where('teacherId=%d',$Teacher['teacherid'])->select();//获取教师已创建的课程信息，并传递给模板
            $this->assign('lesson',$CreatedLessons);
            $this->display();
        }
        else
        {
            $this->error('你还没有登录，请登录','http://localhost/Nise-zf/index.php/Home/TeacherLogin/index.html');
        }
    }
}