<?php
namespace Home\Controller;
use Think\Controller;
class TeacherLogoutController extends Controller
{
    public function logout()
    {
        $value=cookie('teacher');
        session('teacher',null);//清除session
        cookie('teacher',null);//清除cookie
        $this->success("退出成功，请重新登录",'http://localhost/Nise-zf/index.php/Home/TeacherLogin/index');
    }
}