#Nise-zf
    这是HCI二面任务的项目（PHP后台方向）————学生选课系统，本人的项目名称为“Nise-zf”（伪·正方之意）
    本项目是基于ThinkPHP3.2.3框架开发。
运行环境：和ThinkPHP3.2.3运行环境一致，配置文件为/Home/Conf/config.php。
各控制器功能介绍：
  ChooseLesson：显示当前还有余量的课程以及学生选择课程的控制器
  Common：判断课程时间相同和对用户密码进行加密解密的控制器
  CreateLesson：教师创建新课程的控制器
  DeleteLesson：教师删除现有课程的控制器
  DropCLass：学生退课的控制器
  IndexClass：显示系统首页的控制器
  Register：学生和教师注册的控制器（一般为管理员用）
  Student：显示学生页面的控制器
  StudentLogin：学生登录的控制器
  StudentLogout：学生退出登录的控制器
  Teacher：显示教师页面的控制器
  TeacherLogin：教师登录的控制器
  TeacherLogout：教师退出登录的控制器
系统拥有的页面：
  /index.php  首页
  /StudentLogin/index          学生登录界面
  /Student/page                学生主页
  /ChooseLesson/PrintLesson    学生选择课程页面
  /TeacherLogin/index          教师登录界面
  /CreateLesson/create         教师创建课程页面
  /Register/register           学生/教师注册界面（只能直接访问）
数据库配置（数据库名：nise-zf）
  chooselesson表：存储学生已经选择的课
    lessonId:课程的代码（与totallesson表的lessonId同步）
    studentId：学生代码（与student表的studentId同步）
    teacherId：教师代码（与teacher表、total表的teacherId同步）
  student表：存储学生信息
    studentName：学生姓名
    studentId：学生代码
    EmailAddress：邮箱地址（为唯一登录用户名，不重复）
    password：登录密码（已用Common控制器中的加密函数加密，需用Common控制器中的解密函数解密）
  teacher表：存储教师信息
    teacherName：教师姓名
    teacherId：教师代码（与totallesson表的teacherId同步）
    EmailAddress：邮箱地址（为唯一登录用户名，不重复）
    password:登录密码（已用Common控制器中的加密函数加密，需用Common控制器中的解密函数解密）
  totallesson表：存储所有课程信息
    lessonId：课程代码
    lessonName：课程名称
    teacherId：授课教师代码
    lessonTime：上课时间
    lessonRoom：上课课室
    totalVolume：课程总容量
    leftVolume：课程剩余容量
    （任意两节课的上课时间和上课课室不同时相同）
学生选课逻辑：
学生登录->进入学生主页(/Student/page)->进入选课界面(/Choose/PrintLesson)->选课界面后台(/ChooseLesson/PrintLesson)显示还有余量的课程->学生选择课程->选课后台(/ChooseLesson/choose)在chooselesson表写入选课数据，并在totallesson表修改课程剩余容量->显示有多少门课程选择成功->跳回学生主页，查看当前选择的所有课程
学生退课逻辑：
学生登录->进入学生主页(/Student/page)->查看所有已选课程->在课程的末尾按下“退选”按钮->退课后台(/DropClass/drop)删除chooselesson表中该选课数据，并在totallesson表修改课程容量->显示退课成功->跳回学生主页，查看当前选择的所有课程
教师创建课程逻辑：
教师登录->进入教师主页(/Teacher/page)->进入创建课程界面(/CreateLesson/create)->填写课程信息->创建课程后台判断课程信息无误及无冲突后，在totallesson表中添加该课程记录->显示创建课程成功->跳回教师主页，查看已创建的所有课程
教师删除课程逻辑：
教师登录->进入教师主页(/Teacher/page)->查看所有已创建课程->在课程的末尾按下“删除课程”按钮->删除课程后台(/DeleteLesson/delete)删除totallesson表中该课程记录，并删除chooselesson表中所有已选择该课程的记录->跳回教师主页，查看已创建的所有课程