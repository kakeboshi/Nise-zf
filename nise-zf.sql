-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2016 年 10 月 08 日 17:47
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `nise-zf`
--

-- --------------------------------------------------------

--
-- 表的结构 `chooselesson`
--

CREATE TABLE IF NOT EXISTS `chooselesson` (
  `lessonId` int(11) NOT NULL,
  `studentId` int(11) NOT NULL,
  `teacherId` int(11) NOT NULL,
  KEY `studentId` (`studentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `chooselesson`
--

INSERT INTO `chooselesson` (`lessonId`, `studentId`, `teacherId`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 1),
(4, 1, 1),
(5, 1, 1),
(6, 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `studentName` varchar(20) NOT NULL,
  `studentId` int(6) NOT NULL AUTO_INCREMENT,
  `EmailAddress` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`studentId`),
  UNIQUE KEY `EmailAddress` (`EmailAddress`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `student`
--

INSERT INTO `student` (`studentName`, `studentId`, `EmailAddress`, `password`) VALUES
('张烜', 1, '1269651746@qq.com', 'zhangxuan');

-- --------------------------------------------------------

--
-- 表的结构 `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `teacherName` varchar(30) NOT NULL,
  `teacherId` int(4) NOT NULL AUTO_INCREMENT,
  `EmailAddress` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`teacherId`),
  UNIQUE KEY `EmailAddress` (`EmailAddress`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `teacher`
--

INSERT INTO `teacher` (`teacherName`, `teacherId`, `EmailAddress`, `password`) VALUES
('方向王', 1, '12345678@qq.com', 'fangxiangwang');

-- --------------------------------------------------------

--
-- 表的结构 `totallesson`
--

CREATE TABLE IF NOT EXISTS `totallesson` (
  `lessonId` int(11) NOT NULL AUTO_INCREMENT,
  `lessonName` varchar(30) NOT NULL,
  `teacherId` int(11) NOT NULL,
  `teacherName` varchar(30) NOT NULL,
  `lessonTime` varchar(20) NOT NULL,
  `lessonRoom` varchar(6) NOT NULL,
  `totalVolume` int(11) unsigned NOT NULL,
  `leftVolume` int(11) unsigned NOT NULL,
  PRIMARY KEY (`lessonId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `totallesson`
--

INSERT INTO `totallesson` (`lessonId`, `lessonName`, `teacherId`, `teacherName`, `lessonTime`, `lessonRoom`, `totalVolume`, `leftVolume`) VALUES
(1, '高等数学', 1, '方向王', '周一第1.2节', '4206', 150, 139),
(2, '高等数学', 1, '方向王', '周一第3.4节', '4206', 150, 139),
(3, '高等数学', 1, '方向王', '周一第5.6节', '4206', 150, 144),
(4, '高等数学', 1, '方向王', '周一第7.8节', '4206', 150, 145),
(5, '高等数学', 1, '方向王', '周一第9.10节', '4206', 150, 145),
(6, '高等数学', 1, '方向王', '周一第11.12节', '4206', 150, 146);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
